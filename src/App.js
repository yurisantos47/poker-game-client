import React, { useState, useEffect } from 'react';
import Table from './components/Table';

import './App.css';
const TABLES_URL = 'https://storage.googleapis.com/replaypoker-dummy-api/tables';

const getTableIdParams = () => new URLSearchParams(window.location.search).get('table_id');

const App = () => {
  const [table, setTable] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [hasErrors, setHasErrors] = useState(false);

  useEffect(() => {
    const fetchTable = async () => {
      const tableId = getTableIdParams();

      setIsLoading(true);

      try {
        const response = await fetch(`${TABLES_URL}/${tableId}.json`);
        const data = await response.json();

        setTable(data);
      } catch {
        setHasErrors(true);
      } finally {
        setIsLoading(false);
      }
    };

    fetchTable();
  }, []);

  if (isLoading) {
    return <p className="alert-message">Loading...</p>;
  }

  if (hasErrors) {
    return <p className="alert-message">Failed to fetch table.</p>;
  }

  return (
    <div className="App">
      <Table table={table} />
    </div>
  );
};

export default App;
