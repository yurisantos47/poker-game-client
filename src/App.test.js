import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import fetchMock from 'jest-fetch-mock';
import { mount } from 'enzyme';

import App from './App';

fetchMock.enableMocks();

export const waitForComponentToMount = async (component) => {
  await act(async () => {
    await Promise.resolve(component);
    await new Promise((resolve) => setImmediate(resolve));
    component.update();
  });
};

const tableResponseMock = {
  id: 1,
  state: 'open',
  game: 'holdem',
  blinds: {
    small: 10,
    big: 20,
  },
  seats: [{ id: 0, state: 'available' }],
  currentHand: {
    id: 56829,
    communityCards: [],
    players: [{ seatId: 1, cards: ['X', 'X'], bet: 0 }],
    pots: [],
  },
};

describe('App', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders loading on initial render', async () => {
    fetchMock.mockResponse(JSON.stringify(tableResponseMock));

    const component = mount(<App />);
    expect(component.text()).toEqual('Loading...');
  });

  it('renders errors if failed to fetch table data', async () => {
    fetchMock.mockReject(new Error('error'));

    const component = mount(<App />);
    await waitForComponentToMount(component);
    expect(component.text()).toEqual('Failed to fetch table.');
  });

  it('renders table after data is fetched', async () => {
    fetchMock.mockResponse(JSON.stringify(tableResponseMock));

    const component = mount(<App />);
    await waitForComponentToMount(component);
    expect(component.find('div.Table').exists()).toBeTruthy();
  });
});
