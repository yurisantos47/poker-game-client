function numberToMoney(number, currency = 'USD') {
  return Intl.NumberFormat('en-US', { style: 'currency', currency }).format(number);
}

export default numberToMoney;
