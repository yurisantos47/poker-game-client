import React from 'react';

import numberToMoney from '../../utils/number-to-money';
import './Chips.css';

const Chips = ({ amount }) => {
  if (!amount) return null;

  return <div className="Chips">{numberToMoney(amount)}</div>;
};

export default Chips;
