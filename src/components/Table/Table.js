import React from 'react';
import { tableShape } from '../../types';
import cx from 'classnames';

import Seats from '../Seats';
import Cards from '../Cards';
import Pots from '../Pots';

import './Table.css';

const defaultCurrentHand = { players: [], communityCards: [], pots: [] };

const Table = ({
  table,
  table: { currentHand: { players, communityCards, pots } = defaultCurrentHand },
}) => (
  <div className={cx('Table', table.game)}>
    <div>
      <Seats seats={table.seats} players={players} />
      <Cards values={communityCards} />
      <Pots pots={pots} />
    </div>
  </div>
);

Table.propTypes = {
  table: tableShape.isRequired,
};

export default Table;
